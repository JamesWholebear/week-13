# useEffect

Helpful reading:

* https://overreacted.io/a-complete-guide-to-useeffect/


# Homework:

## THE EMPIRE NEEDS YOUR HELP

Terrorists have destroyed one of our space stations conducting large scale political impact research, and supply chains have been utterly decimated. On a distant planet, far, far, away and perhaps even a long time from now, there's a company that helps people get a solid grasp on the job market so that they can get reskilled and back to work.

Create a job board that displays real job titles and real skills in demand to help our empire bounce back from this recession.

### Requirements

Prereq:

* create a new create-react-app as before for the last 2 weeks
* install the jsonwebtoken npm package (`npm install jsonwebtoken` or `yarn add jsonwebtoken`)
* add this to the top of any component files doing fetching:
```jsx
import jwt from "jsonwebtoken";

// this token gets you access to almost all of Emsi's jpa data
const getToken = () => jwt.sign({
  exp: (() => Math.floor(Date.now() / 1000) + 60)(),
  "iss": "learn-web-dev-camper-key"
}, "web-dev-camper-secret");
```
* use this specific fetch request in your `useEffect` in order to fetch real life emsi data:
```jsx
// The only thing you should change in the fetch itself is where it says `title`, `python`, or `50` to change the search type, text, or limit, respectively.
// Unless you're feeling _really_ brave, don't change title to anything other than `skills`
fetch("https://emsiservices.com/emsi-open-proxy-service/postings/us/taxonomies/title?q=python&limit=50", {
  headers: {
    "Content-Type": "application/json",      
    authorization: `Bearer ${getToken()}`,
  },
}).then(res => res.json()).then(res => console.log("real Emsi data!", res))
```

```jsx
// Feel free to call a state setter instead of a console log
const FetchingComponent = () => {
	const [skills, setSkills] = useState();

	useEffect(() => {
		fetch("https://emsiservices.com/emsi-open-proxy-service/postings/us/taxonomies/skills?q=metal&limit=50", {
		  headers: {
		    "Content-Type": "application/json",      
		    authorization: `Bearer ${getToken()}`,
		  },
		}).then(res => res.json()).then(res => setSkills(res))
	}, [])

	return null;
}
```

* Fetch job title and skills data about job postings _once each_ using one or more `useEffect`s, and store that data in state.
* Display a list of job titles (no more than 100), and allow a user to add those titles to a list that they are "wanting to hire". Feel free to use buttons, links, whatever works best.
* In space, companies change their demands a lot, so every time a user adds a job title, add more skills _however you want_ from your list of fetched skills to a list of in demand skills. Make this random, make this follow a strict pattern, remove old ones or make duplicates, I don't care, just add some skills every time the user adds a job title. (the simplest way to do this would be to listen to the "count" of titles change as a dependency in a useEffect)
* Have a child component that displays the number of in demand jobs, have a useEffect listen for when the number grows or shrinks. When the number is growing, show the number in green, when it's shrinking, show it in red. Feel free to use dynamic css classes or just inline styles. Which ever works.