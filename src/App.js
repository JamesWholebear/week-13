import { useState, useEffect } from "react";
import logo from './logo.svg';
import './App.css';
import jwt from "jsonwebtoken";

const getToken = () => jwt.sign({
  exp: (() => Math.floor(Date.now() / 1000) + 60)(),
  "iss": "learn-web-dev-camper-key"
}, "web-dev-camper-secret");

function App(props) {
  const [data, setData] = useState();
  const [count, setCount] = useState(0);
  const [currentFacet, setCurrentFacet] = useState();
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    fetch("https://emsiservices.com/emsi-open-proxy-service/postings/us/meta", {
      headers: {
        "Content-Type": "application/json",      
        authorization: `Bearer ${getToken()}`,
      },
    }).then(res => res.json()).then(res => setData(res))
  }, [])

  useEffect(() => {
    console.log("The modal was toggled!")
  }, [modalOpen])

  useEffect(() => {
    if (modalOpen) {
      console.log("The modal is now open!")
    } else {
      console.log("the modal is closed")
    }
  }, [modalOpen])

  // if the user click a button to change the count, update the facet!
  useEffect(() => {
    if (data && count > -1) {
      setCurrentFacet(data.data.facets[count]);
    }
  }, [count])

  // make sure that count doesn't drop below 0
  useEffect(() => {
    if (count < 0) {
      setCount(0);
    }
  }, [count])

  return (
    <div className="App">
      <header className="App-header">
        {data ? (
          <button onClick={() => setModalOpen(!modalOpen)}>{modalOpen ? "Close" : "Open"} Modal</button>
        ) : "Loading..."}
        {modalOpen && (
          <Modal>
            <div>
              {data.data.attribution.body}
            </div>
            <hr />
            <div>Select a facet!</div>
            <button onClick={() => setCount(count + 1)}>Increment</button>
            <button onClick={() => setCount(count - 1)}>Decrement</button>
            <div>
              Current Facet: {currentFacet}
            </div>
            {currentFacet && <FacetTracker facet={currentFacet} />}
          </Modal>
        )}
      </header>
    </div>
  );
}

export default App;


const Modal = ({ children }) => (
  <div className="modal">
    {children}
  </div>
);

const FacetTracker = props => {
  const [changes, setChanges] = useState(0)
  useEffect(() => {
    console.log("Remember that time that the facet was changed to " + props.facet + "?")
    setChanges(changes + 1)
  }, [props.facet])

  return `The facet has changed ${changes} times.`
}